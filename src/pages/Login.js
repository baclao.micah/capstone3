

import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { Navigate, Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function Login() {
  const { user, setUser } = useContext(UserContext);
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [isActive, setIsActive] = useState(true);

  const retrieveUserDetails = (token) => {
    fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
      headers: {
        Authorization: `Bearer ${token}`
      }
    })
      .then(res => res.json())
      .then(data => {
        console.log(data);
        setUser({
          id: data._id,
          isAdmin: data.isAdmin
        });
      })
      .catch(error => {
        console.log('Error retrieving user details:', error);
      });
  }

  const handleAuthenticationFailure = () => {
    Swal.fire({
      title: "Authentication failed!",
      icon: "error",
      text: "Check your login credentials and try again!"
    });
  }

  const authenticate = (e) => {
    e.preventDefault();
    fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
      method: "POST",
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        email: email,
        password: password
      })
    })
      .then(res => res.json())
      .then(data => {
        console.log(data);
        if (data.accessToken !== undefined) {
          localStorage.setItem('token', data.accessToken);
          retrieveUserDetails(data.accessToken);
          Swal.fire({
            title: "Login Successful",
            icon: "success",
            text: "Welcome to Kopi Haven!"
          });
        } else {
          handleAuthenticationFailure();
        }
      })
      .catch(error => {
        console.log('Error authenticating user:', error);
        handleAuthenticationFailure();
      });

    setPassword('');
  }

  useEffect(() => {
    setIsActive(email !== '' && password !== '');
  }, [email, password]);

  return (
    user.id !== null ? (user.isAdmin ? <Navigate to="/admin" /> : <Navigate to="/products/active" />) :
      <div className="login-container">
        <div className="login-form">
          <h1 style={{ textAlign: 'center', marginBottom: '3rem' }}>Login</h1>
          <Form onSubmit={authenticate}>
            <Form.Group className="my-3" controlId="userEmail">
              <Form.Label>Email address</Form.Label>
              <Form.Control
                type="email"
                placeholder="Enter email"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
                required
              />
            </Form.Group>

            <Form.Group className="mb-3" controlId="password">
              <Form.Label>Password</Form.Label>
              <Form.Control
                type="password"
                placeholder="Password"
                value={password}
                onChange={(e) => setPassword(e.target.value)}
                required
              />
            </Form.Group>

            <Button variant="warning" type="submit" id="submitBtn" disabled={!isActive}>
              Submit
            </Button>
          </Form>
          
          <div style={{ marginTop: '1rem' }}>
            No account yet?{' '}
            <Link to="/register" className="register-link">
              Register here
            </Link>
          </div>
        </div>
      </div>
  );
}
