import '../App.css';
import { Fragment, useEffect, useState, useRef } from 'react';
import { Carousel } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { FaArrowDown } from 'react-icons/fa';

export default function Home() {
  const [fadeIn, setFadeIn] = useState(false);
  const scrollRef = useRef(null);

  useEffect(() => {
    setFadeIn(true);
  }, []);

  const products = [
    {
      id: "64806ffcb2fb4b8c0d21a58a",
      name: "Classic Brew",
      description: "Enjoy the timeless elegance of our smooth and balanced traditional coffee.",
      price: 130,
      onOffer: true,
      image: "https://www.starbucksathome.com/ca/sites/default/files/styles/recipe_ingredient/public/2021-03/Starbucks%20Iced%20Coffee_2.jpg?itok=0iwwKbgv"
    },
    {
      id: "64807016b2fb4b8c0d21a58c",
      name: "Creamy Caramel",
      description: "Delight in the luscious blend of caramel notes infused into our aromatic coffee.",
      price: 140,
      onOffer: true,
      image: "https://www.brighteyedbaker.com/wp-content/uploads/2020/04/Caramel-Iced-Coffee-3.jpg"
    },
    {
      id: "64807033b2fb4b8c0d21a58e",
      name: "Vanilla Bliss",
      description: "Immerse yourself in the comforting embrace of our vanilla-infused coffee.",
      price: 140,
      onOffer: true,
      image: "https://cdn.apartmenttherapy.info/image/upload/v1634135326/k/Photo/Recipe%20Ramp%20Up/2021-10-Vanilla-Syrup/vanilla_syrup_pour_web_01.jpg"
    },
    {
      id: "64807048b2fb4b8c0d21a590",
      name: "Decadent Irish Cream",
      description: "Unwind with the smooth and luxurious taste of Irish cream blended into our coffee.",
      price: 185,
      onOffer: true,
      image: "https://www.thespeckledpalate.com/wp-content/uploads/2021/03/The-Speckled-Palate-Baileys-Irish-Cream-Coffee-Photo-720x720.jpg"
    }
  ];

  const handleArrowClick = () => {
    if (scrollRef.current) {
      window.scrollTo({
        top: scrollRef.current.offsetTop,
        behavior: 'smooth',
      });
    }
  };

  return (
    <Fragment>

    <div className="home-page">
      <h1>Welcome to Kopi Haven</h1>
      <p className={fadeIn ? 'fade-in' : ''}>Your daily dose of coffee magic awaits!</p>

      <div className="scroll-down-container">
        <div className="scroll-down-icon" onClick={handleArrowClick}>
          <FaArrowDown />
        </div>
      </div>
    </div>

    {/* Carousel Start */}

    <div className="carousel-container" ref={scrollRef}>
      <h1 className="menu-heading">Flavors to Savor</h1>
      <Carousel>
        {products.map((product) => (
          <Carousel.Item key={product.id}>
            <Link to={`/products/${product.id}`}>
              <div className="carousel-image-container">
                <img
                  className="carousel-image"
                  src={product.image}
                  alt={product.name}
                />
              </div>
              <Carousel.Caption>
                <h3>{product.name}</h3>
                <p>{product.description}</p>
              </Carousel.Caption>
            </Link>
          </Carousel.Item>
        ))}
      </Carousel>
    </div>

    {/* Carousel End */}

  </Fragment>
  );
}