import '../App.css';
import Container from 'react-bootstrap/Container';
import { Fragment, useContext } from 'react';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import { Link, NavLink, useNavigate } from 'react-router-dom';
import UserContext from '../UserContext';

export default function AppNavbar() {
  const { user } = useContext(UserContext);
  const navigate = useNavigate();

  // Function to handle navbar collapse
  const handleNavbarCollapse = () => {
    const navbar = document.querySelector('.navbar-collapse');
    if (navbar.classList.contains('show')) {
      navbar.classList.remove('show');
    }
  };

  // Function to navigate and handle navbar collapse
  const handleNavLinkClick = () => {
    handleNavbarCollapse();
    navigate('/');
  };

  return (
    <Navbar collapseOnSelect className="custom-navbar" variant="dark" expand="lg">
      <Container fluid>
        <Navbar.Brand as={Link} to="/" onClick={handleNavbarCollapse}>
           <img src={require('./logo.gif')} alt="Kopi Haven" />
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="ms-auto">
            <Nav.Link as={NavLink} to="/" onClick={handleNavLinkClick}>
              Home
            </Nav.Link>
            {user.isAdmin ? (
              <Nav.Link as={NavLink} to="/admin" end onClick={handleNavLinkClick}>
                Admin Dashboard
              </Nav.Link>
            ) : (
              <Nav.Link as={NavLink} to="/products/active" end onClick={handleNavLinkClick}>
                Products
              </Nav.Link>
            )}
            {user.id !== null && (
              <Nav.Link as={NavLink} to="/users/details" onClick={handleNavLinkClick}>
              Orders
              </Nav.Link>
              )}
            {user.id !== null ? (
              <Nav.Link as={NavLink} to="/logout" onClick={handleNavLinkClick}>
                Logout
              </Nav.Link>
            ) : (
              <Fragment>
                <Nav.Link as={NavLink} to="/login" onClick={handleNavLinkClick}>
                  Login
                </Nav.Link>
                <Nav.Link as={NavLink} to="/register" onClick={handleNavLinkClick}>
                  Register
                </Nav.Link>
              </Fragment>
            )}
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
}
